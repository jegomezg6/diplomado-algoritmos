package main;

import java.util.ArrayList;
import java.util.List;

public class main {

	public static void main(String[] args) {
		List<Integer> a1 = new ArrayList<>();
		List<Integer> a2 = new ArrayList<>();

		a1.add(1);
		a1.add(2);
		a1.add(4);
		a1.add(5);
		a1.add(8);

		a2.add(4);
		a2.add(6);
		a2.add(7);
		a2.add(8);

		mergeSort(a1, a2);

	}

	public static void mergeSort(List<Integer> a1, List<Integer> a2) {

		int[] ordenado = new int[a1.size() + a2.size()];
		int contador = 0;
		int total = 0;

		for (int i = 0; i < a1.size(); i++) {
			for (int j = contador; j < a2.size(); j++) {
				if (a1.get(i) < a2.get(j)) {
					ordenado[total] = a1.get(i);
					total++;
					break;
				} else {
					ordenado[total] = a2.get(j);
					total++;
					contador++;
				}

			}

			if (contador == a2.size()) {

				ordenado[total] = a1.get(i);
				total++;
			}

		}

		for (int i : ordenado) {
			System.out.print(i);
		}

	}
}
