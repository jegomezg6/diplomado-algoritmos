package arbol;

import javax.xml.soap.Node;

public class Arbol {

	Nodo cabeza = null;
	private int contador = 0;

	public void insert(int value) {
		if (cabeza == null) {
			cabeza = new Nodo(value);
		} else {
			insertNodo(cabeza, value);
		}
		contador++;
	}

	public void insertNodo(Nodo actual, int value) {

		if (value < actual.getValue()) {
			if (actual.getLeft() == null) {
				actual.setLeft(new Nodo(value));
			} else {
				insertNodo(actual.getLeft(), value);
			}
		} else {
			if (actual.getRight() == null) {
				actual.setRight(new Nodo(value));
			} else {
				insertNodo(actual.getRight(), value);
			}
		}

	}

	public boolean Contains(Nodo actual, int value) {
		if (actual == null) {
			return false;
		}

		if (actual.getValue() == value) {
			return true;
		}

		if (value < actual.getValue()) {
			return Contains(actual.getLeft(), value);
		} else {
			return Contains(actual.getRight(), value);
		}
	}

	public Nodo findNode(Nodo actual, int value) {
		if (actual == null) {
			return null;
		}
		if (actual.getValue() == value) {
			return actual;
		}
		if (value < actual.getValue()) {
			return findNode(actual.getLeft(), value);
		} else {
			return findNode(actual.getRight(), value);
		}
	}

	public Nodo findParent(Nodo actual, int value) {

		if (actual == null) {
			return null;
		}

		if (actual.getValue() == value) {
			return null;
		}

		if (value < actual.getValue()) {
			if (null == actual.getLeft()) {
				return null;
			} else if (actual.getLeft().getValue() == value) {
				return actual;
			} else {
				return findParent(actual.getLeft(), value);
			}

		} else {
			if (null == actual.getRight()) {
				return null;
			} else if (actual.getRight().getValue() == value) {
				return actual;
			} else {
				return findParent(actual.getRight(), value);
			}
		}

	}

	public boolean remover(int value) {
		Nodo removido = findNode(cabeza, value);

		if (removido == null) {
			return false;
		}

		if (contador == 1) {
			cabeza = null;
		} else {
			Nodo padre = findParent(cabeza, value);
			// caso #1 nodo hoja
			if (removido.getLeft() == null && removido.getRight() == null) {

				if (removido.getValue() < padre.getValue()) {
					padre.setLeft(null);
				} else {
					padre.setRight(null);
				}

			} else if (removido.getLeft() == null && removido.getRight() != null) {
				// caso #2
				if (removido.getValue() < padre.getValue()) {
					padre.setLeft(removido.getRight());
				} else {
					padre.setRight(removido.getRight());
				}
			} else if (removido.getLeft() != null && removido.getRight() == null) {
				// caso #3
				if (removido.getValue() < padre.getValue()) {
					padre.setLeft(removido.getLeft());
				} else {
					padre.setRight(removido.getLeft());
				}

			} else {
				// caso #4
				Nodo mayorValor= removido.getLeft();
				while (null != mayorValor.getRight()) {
					
					mayorValor= mayorValor.getRight();
				}
				
				findParent(cabeza, mayorValor.getValue()).setRight(null);
				removido.setValue(mayorValor.getValue());
				
			}

		}
		contador--;
		return true;
	}
	
	
	public int findMin(Nodo actual) {
		if (actual.getLeft()==null) {
			return actual.getValue();
		}
		return findMin(actual.getLeft());
	}
	
	public int findMax(Nodo actual) {
		if (actual.getRight()==null) {
			return actual.getValue();
		}
		return findMax(actual.getRight());
	}
	
	
	public void preOrden(Nodo actual) {
		if(actual!=null) {
			System.out.println(actual.getValue());
			preOrden(actual.getLeft());
			preOrden(actual.getRight());
		}
		
	}
	public void postOrden(Nodo actual) {
		if(actual!=null) {
			postOrden(actual.getLeft());
			postOrden(actual.getRight());
			System.out.println(actual.getValue());
		}
		
	}

}
