package main;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class Runner {

	public static void main(String[] args) {

		int[][] matrix = { 
				{ 0, 1, 0, 0, 1, 0, 0, 0 },
				{ 1, 0, 0, 0, 0, 1, 0, 0 }, 
				{ 0, 0, 0, 1, 0, 1, 1, 0 },
				{ 0, 0, 1, 0, 0, 0, 1, 1 }, 
				{ 1, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 1, 1, 0, 0, 0, 1, 0 },
				{ 0, 0, 1, 1, 0, 1, 0, 1 }, 
				{ 0, 0, 0, 1, 0, 0, 1, 0 } 
				};

		bfs(matrix, 1, 7);

	}

	public static void bfs(int[][] marix, int inicial, int fin) {

		int tamano = marix.length;
		int[] color = new int[tamano];
		int[] distancia = new int[tamano];
		int[] pre = new int[tamano];

		Stack<Integer> pila = new Stack<>();
		Queue<Integer> cola = new LinkedList<>();

		for (int i = 0; i < tamano; i++) {
			color[i] = 0;
			distancia[i] = Integer.MAX_VALUE;
			pre[i] = -1;
		}

		color[inicial] = 1;
		distancia[inicial] = 0;

		cola.clear();
		cola.add(inicial);

		while (!cola.isEmpty()) {
			int actual = cola.poll();

			for (int j = 0; j < tamano; j++) {

				if (marix[actual][j] == 1) {

					if (color[j] == 0) {
						color[j] = 1;
						distancia[j] = distancia[actual] + 1;
						pre[j] = actual;
						cola.add(j);
					}
					color[actual] = 2;
				}

			}

		}

		System.out.println("la distacia es: " + distancia[fin]);

		int prede = pre[fin];
		pila.push(fin);
		while (prede != -1) {
			pila.push(prede);
			prede = pre[prede];
		}

		System.out.print("EL CAMINO ES: ");
		while (!pila.isEmpty()) {
			System.out.print(pila.pop() + " ");
		}

	}

}
