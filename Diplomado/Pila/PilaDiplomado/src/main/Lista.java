package main;

public class Lista {
	private Node head = null;
	private Node tail = null;

	public void push(int value) {
		Node n = new Node(value);
		if (head == null) {
			head = n;
			tail = n;
		} else {
			head = n;
			head.setSiguiente(tail);
			tail = n;
		}
	}

	public void Peek() {
		if (head != null) {
			System.out.println(head.getValor());
		} else {
			System.out.println("La cola esta vacia");
		}
	}

	public void pop() {

		if (head == null) {
			System.out.println("La cola esta vacia");
		} else {
			head = head.getSiguiente();
			tail = head;
		}
	}
	
	public void isEmpty() {
			System.out.println(head == null);
	}

}
