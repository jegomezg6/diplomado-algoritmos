package main;

import java.util.Stack;

public class Runner {

	public static void main(String[] args) {
		String s1 = "casota";
		String s2 = "zapote";

		int distance = levenshteinDistence(s1, s2);
		System.out.println("Total de operaciones: " + distance);

	}

	public static int levenshteinDistence(String s1, String s2) {
		Stack<String> stack = new Stack<>();
		int n = s1.length();
		int m = s2.length();

		int[][] matrix = new int[n + 1][m + 1];

		for (int k = 0; k <= m; k++) {
			matrix[0][k] = k;
		}

		for (int k = 0; k <= n; k++) {
			matrix[k][0] = k;
		}

		for (int k = 1; k <= n; k++) {
			for (int j = 1; j <= m; j++) {
				int costo = 1;
				if (s1.charAt(k - 1) == s2.charAt(j - 1)) {
					costo = 0;
				}
				matrix[k][j] = Math.min(matrix[k][j - 1] + 1,
						Math.min(matrix[k - 1][j] + 1, matrix[k - 1][j - 1] + costo));
			}
			System.out.println();
		}

		int p = n;
		int q = m;
		while (p > 0 && q > 0) {

			if (matrix[p - 1][q - 1] == matrix[p][q] ||matrix[p - 1][q - 1] + 1 == matrix[p][q]  ) {
				if (matrix[p - 1][q - 1] + 1 == matrix[p][q]) {
					stack.push("cambie: " + s1.charAt(p - 1) + " por " + s2.charAt(q - 1));
				}
				q = q - 1;
				p = p - 1;

			} else if (matrix[p][q - 1] + 1 == matrix[p][q]) {
				stack.push("inserte: " + s2.charAt(q - 1));
				q = q - 1;

			} else {
				stack.push("elimine: " + s1.charAt(p - 1));
				p = p - 1;

			}

		}
		while (!stack.isEmpty()) {
			System.out.println(stack.peek());
			stack.pop();
		}

		for (int k = 0; k <= n; k++) {
			for (int j = 0; j <= m; j++) {
				System.out.print(matrix[k][j] + " ");
			}
			System.out.println();
		}

		return matrix[n][m];
	}

}
