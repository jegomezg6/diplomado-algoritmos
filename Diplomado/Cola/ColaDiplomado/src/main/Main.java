package main;

public class Main {

	public static void main(String[] args) {
		Lista cola = new Lista();

		cola.Enqueue(5);
		cola.Enqueue(4);
		cola.Enqueue(3);
		cola.Enqueue(2);
		cola.Peek();
		cola.Denqueue();
		cola.Peek();
		cola.Denqueue();
		cola.Peek();
		cola.Denqueue();
		cola.Enqueue(6);
		cola.Peek();
		cola.Denqueue();
		cola.Peek();
	}

}
