package main;

public class Node {
	private int valor;
	private Node siguiente;
	

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}

	public Node getSiguiente() {
		return siguiente;
	}

	public void setSiguiente(Node siguiente) {
		this.siguiente = siguiente;
	}

	public Node(int valor) {
		this.valor = valor;
	}

	
	
	

}
