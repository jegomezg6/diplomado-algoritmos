package main;

public class Lista {
	private Node head = null;
	private Node tail = null;

	public void Enqueue(int value) {
		Node n = new Node(value);
		if (head == null) {
			head = n;
			tail = n;
		} else {
			tail.setSiguiente(n);
			tail = n;
		}
	}

	public void Peek() {
		if (tail != null) {
			System.out.println(head.getValor());
		} else {
			System.out.println("La cola esta vacia");
		}
	}

	public void Denqueue() {

		if (head == null) {
			System.out.println("La cola esta vacia");
		} else {
			head = head.getSiguiente();
		}
	}

}
